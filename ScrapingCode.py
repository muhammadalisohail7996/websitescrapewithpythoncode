#!/usr/bin/env python
# coding: utf-8

# In[16]:


import ssl
from html.parser import HTMLParser
import urllib.request

# Create an unverified SSL context
context = ssl._create_unverified_context()

# Making a GET request with the unverified context
response = urllib.request.urlopen('https://careers.umt.edu.pk/', context=context)

# Read the HTML content
html_content = response.read()

# Define a custom HTML parser to extract text
class TextExtractor(HTMLParser):
    def __init__(self):
        super().__init__()
        self.text_content = []

    def handle_data(self, data):
        self.text_content.append(data)

# Parse the HTML content without BeautifulSoup
parser = TextExtractor()
parser.feed(html_content.decode('utf-8'))

# Join the extracted text content
text_content = ' '.join(parser.text_content)

# Print the extracted text
print(text_content)


# In[ ]:





# In[ ]:




